# README #

This is a Multitouch, Multiselection EventSystem built on top of UnityEngine.UI. The multitouch system is contained in the Assets/Plugins/Multitouch directory. The rest of the repo is a Unity project demonstrating its use.

See the post at https://blogs.msdn.microsoft.com/dwim/2016/11/15/multitouch-event-system-for-unity-5-5/ for more information

branches

 * 5.5, tag: Unity5.5-b11 The version for Unity 5.5 Beta 11
 * 5.5, tag: Unity5.5-b10 The version for Unity 5.5 Beta 10
 * 5.3, 5.4: versions for use with unmodified Unity 5.3.x and 5.4.x
 * 5.3.testonly, 5.4.testonly: Just the test app for use with https://bitbucket.org/johnseghersmsft/ui.

For information about using the Custom UnityEngine.UI built from https://bitbucket.org/johnseghersmsft/ui, along with the *.testonly branches, see blog posts at https://blogs.msdn.microsoft.com/dwim/2016/10/31/multitouch-event-system-for-unity/ and https://blogs.msdn.microsoft.com/dwim/2016/11/04/multitouch-event-system-for-unity-5-4-x/ for more information on this usage.


### Who do I talk to? ###

* johnse@microsoft.com